angular.module 'Wicked', []

angular.module('Wicked').controller('PdfCtrl', ($scope,$http) ->
  $scope.plainMessage = "This message created in the controller"

  $http.jsonp("http://localhost:3000/people/1.json?callback=JSON_CALLBACK").success((data) ->
    $scope.person = data
  )
)
