class MainController < ApplicationController
  respond_to :html, :pdf

  def index
    respond_to do |format|
      format.pdf do
        render  :pdf => "testfile",
                :javascript_delay => 15000,
                :show_as_html => params[:debug].present?
      end
      format.html
    end

  end
end
