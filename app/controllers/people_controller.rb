class PeopleController < ApplicationController
  respond_to :json

  def show
    @person = Person.find(params[:id])

    respond_with @person, :callback => params['callback']
  end
end
